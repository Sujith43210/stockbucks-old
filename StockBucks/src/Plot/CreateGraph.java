package Plot;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Paint;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.RenderingHints.Key;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.AttributedCharacterIterator;
import java.util.Date;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.LegendItemEntity;
import org.jfree.chart.labels.XYSeriesLabelGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;


public class CreateGraph {	

	private ChartPanel chartPanel = createChart();
		
public void startgraph(){			
		
	 JFrame jf= new JFrame();
		jf.setTitle("Practice Graph");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setLayout(new BorderLayout(0,0));
		jf.add(chartPanel, BorderLayout.CENTER);
		chartPanel.setMouseWheelEnabled(true);
		chartPanel.setInitialDelay(0);
		chartPanel.setDismissDelay(10000);	
		creatett(0);
		creatett(1);
		JPanel p=new JPanel(){
			public void paintComponent(Graphics g)
			{
				g.setColor(new Color(0,0,0,140));
				g.fillRect(0,0,getWidth(),getHeight());
			}
		};
		p.setOpaque(false);
	//	p.setLayout(new GridBagLayout());
		jf.setGlassPane(p);
		
		chartPanel.addChartMouseListener(new ChartMouseListener() {			
			@Override
			public void chartMouseMoved(ChartMouseEvent arg0) {	}			
						
			@Override
			public void chartMouseClicked(ChartMouseEvent clk) {				
				
				ChartEntity chartentity= clk.getEntity();
				if(chartentity.getToolTipText()!= null && chartentity.getToolTipText().contains("AxisBnk")){
					System.out.println("Launching AxisBnk...");
					p.setVisible(true);	
					AxisBnk ab= new AxisBnk();	
					p.setVisible(false);
				}				
				else if(chartentity.getToolTipText()!= null && chartentity.getToolTipText().contains("Tata")){
					System.out.println("Launching Tata...");
					p.setVisible(true);	
					Tata t= new Tata();					
					p.setVisible(false);					
				}				
				else if(chartentity instanceof LegendItemEntity){
					
					JFreeChart jfc= chartPanel.getChart();
					XYPlot xyp= jfc.getXYPlot();					
					LegendItemEntity itemEntity = (LegendItemEntity) chartentity;											
					int index= ((XYSeriesCollection) itemEntity.getDataset()).getSeriesIndex(itemEntity.getSeriesKey());
					XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) xyp.getRenderer();
					if(renderer.getItemLineVisible(index, index)){
						renderer.setSeriesLinesVisible(index, false);				
						renderer.setSeriesToolTipGenerator(index, null);
					}
					else{
					creatett(index);
					renderer.setSeriesLinesVisible(index, true);
					}						
				}						
									}			
		});
		
		ClockLabel clock = new ClockLabel();
	    JPanel customPanel1 = new JPanel();
	    customPanel1.setBackground(Color.WHITE);
	    clock.setFont(new Font("Monotype Corsiva",1,28));
	    clock.setPreferredSize(new Dimension(400,20));
	    customPanel1.add(clock);
	    jf.add(customPanel1, BorderLayout.SOUTH);
	    
	    JPanel customPanel = new JPanel();
        String s = "Welcome To StockBucks";
        JLabel f = new JLabel(s);        
        MarqueePanel mp = new MarqueePanel(s, 30);
        customPanel.add(mp);
        customPanel.setBackground(Color.WHITE);
        f.setPreferredSize(new Dimension(400,30));
        jf.add(customPanel,BorderLayout.NORTH);
        mp.start();
		
		jf.setSize(1000,700);
		jf.setLocationRelativeTo(null);	
        jf.setVisible(true);        
	}
	
	public XYDataset createDataset() {		
		
		XYSeries series1= new XYSeries("AxisBnk");
		XYSeries series2= new XYSeries("Tata");		
		try{
			Class.forName("com.mysql.jdbc.Driver"); 			  
			Connection con=DriverManager.getConnection(  
			"jdbc:mysql://localhost:3306/testdb1","root",""); 			
			Statement stmt= con.createStatement();
			ResultSet rs= stmt.executeQuery("Select AxisBnk, Tata, Time FROM testtable2");
			while(rs.next()){ 				
				series1.add(rs.getInt("Time"), rs.getInt("AxisBnk")*0.12);
				series2.add(rs.getInt("Time"), rs.getInt("Tata")*0.12);	
				}						
		}
		catch(Exception e){e.printStackTrace();
		System.out.println("Cannot connect to database!");}		
	    XYSeriesCollection dataset= new XYSeriesCollection();		
		dataset.addSeries(series1);
		dataset.addSeries(series2);		
  return dataset;   
    }	
	
	public ChartPanel createChart(){		
		XYDataset dat= createDataset();
		JFreeChart newchart= ChartFactory.createTimeSeriesChart("Normalized Graph", "Time", "Price", dat);
		LegendTitle legend= newchart.getLegend();
		legend.setPosition(RectangleEdge.RIGHT);
		XYPlot xyplot= newchart.getXYPlot();	
		XYLineAndShapeRenderer rend= new XYLineAndShapeRenderer();
		rend.setSeriesPaint(0, Color.MAGENTA);
		rend.setSeriesPaint(1, Color.BLACK);
		rend.setBaseShapesVisible(false);
		rend.setDefaultEntityRadius(20);		
		rend.setLegendItemToolTipGenerator(new XYSeriesLabelGenerator() {			
			@Override
			public String generateLabel(XYDataset dataset, int series) {				
				return "Click on a graph to hide/display it.";
			}});				
		xyplot.setBackgroundPaint(Color.lightGray);			
		xyplot.setRenderer(rend);		
	//	ValueMarker bullbandlow = new ValueMarker(400.00, Color.yellow, new BasicStroke(1.0f));
	//	ValueMarker bullbandhigh = new ValueMarker(600.00, Color.yellow, new BasicStroke(1.0f));
	//	bullbandhigh.setLabel("BullBand- High");
	//	bullbandlow.setLabel("Bullband- Low");
	//	bullbandhigh.setLabelOffset(new RectangleInsets(10d,40d,20d,20d));
	//	bullbandlow.setLabelOffset(new RectangleInsets(10d,40d,20d,20d));	
	//	xyplot.addRangeMarker(bullbandlow);
	//	xyplot.addRangeMarker(bullbandhigh);	
		xyplot.setDomainGridlinesVisible(false);
		xyplot.setRangeGridlinesVisible(false);		
	
		final IntervalMarker target = new IntervalMarker(15.0, 21.0);
        target.setLabel("Bull Band");
        target.setLabelFont(new Font("SansSerif", Font.ITALIC, 11));
        target.setLabelAnchor(RectangleAnchor.LEFT);
        target.setLabelTextAnchor(TextAnchor.CENTER_LEFT);
        target.setPaint(new Color(222, 222, 255, 128));
        xyplot.addRangeMarker(target, Layer.BACKGROUND);		
		return new ChartPanel(newchart);		
	}	
	
	public void creatett(int t){
		JFreeChart jfc= chartPanel.getChart();
		XYPlot xyp= jfc.getXYPlot();
		XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) xyp.getRenderer();
		switch(t){	
		
		case 0:		
		 XYToolTipGenerator tt0 = new XYToolTipGenerator() {
	         public String generateToolTip(XYDataset dataset, int series, int item) {
	            Number x = dataset.getX(series, item);
	            Double y = (Double) dataset.getY(series, item)/0.12;
	            return "AxisBnk: ("+x.toString()+","+ y.toString()+")";
	         }
	      };
	      renderer.setSeriesToolTipGenerator(t, tt0);
	      break;
	      
		case 1:
			 XYToolTipGenerator tt1 = new XYToolTipGenerator() {
		         public String generateToolTip(XYDataset dataset, int series, int item) {
		            Number x = dataset.getX(series, item);
		            Double y = (Double) dataset.getY(series, item)/0.12;
		            return "Tata: ("+x.toString()+","+ y.toString()+")";
		         }
		      };
		      renderer.setSeriesToolTipGenerator(t, tt1);
		      break;			
		}		
	}	
	
	public static void main(String[] args){	
		CreateGraph cg= new CreateGraph();
		cg.startgraph();
		
	}
}	
	
	class ClockLabel extends JLabel implements ActionListener {

		private static final long serialVersionUID = 1L;

		public ClockLabel() {
		    super("" + new Date());
		    Timer t = new Timer(1000, this);
		    t.start();
		  }

		  public void actionPerformed(ActionEvent ae) {
		    setText((new Date()).toString());
		  }
		}
	
	class MarqueePanel extends JPanel implements ActionListener {

		   private static final long serialVersionUID = 1L;
			private static final int RATE = 12;
		    private final Timer timer = new Timer(2000 / RATE, this);
		    private final JLabel label = new JLabel();
		    private final String s;
		    private final int n;
		    private int index;

		    public MarqueePanel(String s, int n) {
		        if (s == null || n < 1) {
		            throw new IllegalArgumentException("Null string or n < 1");
		        }
		        StringBuilder sb = new StringBuilder(n);
		        for (int i = 0; i < n; i++) {
		            sb.append(' ');
		        }
		        this.s = sb + s + sb;
		        this.n = n;
		        label.setFont(new Font("Monotype Corsiva", Font.ITALIC, 24));
		        label.setForeground(Color.RED);
		        label.setText(sb.toString());
		        this.add(label);
		    }

		    public void start() {
		        timer.start();
		    }

		    public void stop() {
		        timer.stop();
		    }

		    public void actionPerformed(ActionEvent e) {
		        index++;
		        if (index > s.length() - n) {
		            index = 0;
		        }
		        label.setText(s.substring(index, index + n));
		    }
	}
	
	





package Plot;

import java.awt.BorderLayout;
import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JDialog;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class AxisBnk {
	
	private ChartPanel chartPanel = createchart();
	
	public ChartPanel createchart(){
	XYDataset dset= createdataset();
	JFreeChart newchart= ChartFactory.createTimeSeriesChart("AxisBnk", "Time", "Price", dset);
	XYPlot xyplot= newchart.getXYPlot();
	XYLineAndShapeRenderer rend= new XYLineAndShapeRenderer();
	rend.setBaseToolTipGenerator(new StandardXYToolTipGenerator());
	rend.setBaseShapesVisible(false);
	rend.setDefaultEntityRadius(20);
	xyplot.setBackgroundPaint(Color.cyan);			
	xyplot.setRenderer(rend);
	xyplot.setDomainGridlinesVisible(false);
	xyplot.setRangeGridlinesVisible(false);
	return new ChartPanel(newchart);	
	}
	
	public XYDataset createdataset(){
		
		XYSeries series1= new XYSeries("AxisBnk");
		
		try{
			Class.forName("com.mysql.jdbc.Driver"); 			  
			Connection con=DriverManager.getConnection(  
			"jdbc:mysql://localhost:3306/testdb1","root",""); 			
			Statement stmt= con.createStatement();
			ResultSet rs= stmt.executeQuery("Select Value, Time FROM axisbnk");
			while(rs.next()){ 				
				series1.add(rs.getInt("Time"), rs.getInt("Value"));				
			}			
		}
		catch(Exception e){e.printStackTrace();}
		return new XYSeriesCollection(series1);
	}
	
	public AxisBnk(){	
		
		JDialog jd= new JDialog();	
		jd.setTitle("Practice Graph");
		jd.setLayout(new BorderLayout(0,0));
		jd.add(chartPanel, BorderLayout.CENTER);
		chartPanel.setInitialDelay(0);
		chartPanel.setMouseWheelEnabled(true);
		chartPanel.setDismissDelay(10000);
		jd.setModal(true);
		jd.setSize(1000,700);
		jd.setLocationRelativeTo(null);
	    jd.setVisible(true);		
	
	}
}
